import React from 'react';
import './App.scss';

function App() {
  return (
    <div className="App">
        <button onClick={() => alert('hello')}>Let's go</button>
    </div>
  );
}

export default App;
